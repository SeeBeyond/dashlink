<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->foreignId('owner_id')->nullable();
            $table->integer('user_id')->unique();
            $table->integer('account_id')->nullable();

            $table->integer('stars')->nullable();
            $table->integer('demons')->nullable();
            $table->integer('creator_points')->nullable();
            $table->integer('coins_user')->nullable();
            $table->integer('coins_secret')->nullable();
            $table->integer('diamonds')->nullable();

            $table->string('twitch')->nullable();
            $table->string('twitter')->nullable();
            $table->string('youtube')->nullable();

            $table->string('plus_bio')->nullable();
            $table->string('plus_discord')->nullable();
            $table->string('plus_website')->nullable();
            $table->string('plus_video')->nullable();
            $table->string('plus_email')->nullable();
            $table->string('plus_pronouns')->nullable();
            $table->string('plus_location')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
};
