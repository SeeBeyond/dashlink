<?php

namespace App\Actions;

use App\Models\Profile;
use GdImage;
use Illuminate\Support\Facades\Http;
use Storage;

class CreateCard
{
    private string $font = './../resources/fonts/pusab.otf';

    private GdImage $card;

    private int $cardWidth;

    public function show($name) {
        if(Storage::disk('contabo')->missing('cards/'.strtolower($name).'png')) {
            $profile = GetProfile::byName($name);
            $this->create($profile);
        }

        $p = 'filesystems.disks.contabo.';
        $url = config($p.'endpoint') . '/' . config($p.'tenant') . ':' . config($p.'bucket') . '/cards/' . strtolower($name) . '.png';
        return redirect($url);
    }

    /**
     * Generate profile card as PNG
     *
     */
    public function create(Profile $profile)
    {
        $this->card = $this->import('card');
        imagealphablending($this->card,true);
        imagesavealpha($this->card,true);
        $this->cardWidth = imagesx($this->card);


        $this->printStat('star', $profile->stars, 0, 120);
        $this->printStat('diamond', $profile->diamonds, 1, 110);
        $this->printStat('coin', $profile->coins_secret, 2, 50);
        $this->printStat('silvercoin', $profile->coins_user, 3, 50);
        $this->printStat('demon', $profile->demons, 4, 10);

        $this->print($profile->name, 50, $this->cardWidth / 2, 90, 1);

        $tmp = tempnam(sys_get_temp_dir(), 'card_');
        imagepng($this->card, $tmp);
        imagedestroy($this->card);
        Storage::disk('contabo')->putFileAs("cards/" , $tmp, strtolower($profile->name) . '.png', 'public');
        unlink($tmp);
    }

    private function import($name): GdImage|bool
    {
        return imagecreatefrompng("./../resources/img/$name.png");
    }

    private function print($text, $size, $x, $y, $align = 0) {
        $white = imagecolorallocate($this->card, 255, 255, 255);
        $black = imagecolorallocate($this->card, 0, 0, 0);
        $shadow = imagecolorallocate($this->card, 92, 51, 31);
        switch($align) {
            case 1:
                // Center Align
                $x = $x - $this->textWidth($text, $size);
                break;
            case 2:
                // Right Align
                $x = $x - $this->textWidth($text, $size) * 2;
                break;
        }
        $this->imagettfstroketext($this->card, $size, 0, $x + 4, $y + 4, $shadow, $shadow, $this->font, $text, 3);
        $this->imagettfstroketext($this->card, $size, 0, $x, $y, $white, $black, $this->font, $text, 3);
    }

    private function printStat($name, $value, $it, $offset = 0) {
        $x = $offset + (($this->cardWidth - 200) / 5) * $it;
        $icon = $this->import($name);
        imagecopy($this->card, $icon, 200 + $x, 140, 0, 0, 75, 75);
        imagedestroy($icon);
        $this->print($value, 32, 190 + $x, 195, 2);
    }

    private function textWidth($text, $size) {
        list($left, , $right, , , ) = imageftbbox($size, 0, $this->font, $text);
        return ($right - $left) / 2;
    }

    // http://www.johnciacia.com/2010/01/04/using-php-and-gd-to-add-border-to-text/
    private function imagettfstroketext(&$image, $size, $angle, $x, $y, &$textcolor, &$strokecolor, $fontfile, $text, $px) {
        for($c1 = ($x-abs($px)); $c1 <= ($x+abs($px)); $c1++)
            for($c2 = ($y-abs($px)); $c2 <= ($y+abs($px)); $c2++)
                $bg = imagefttext($image, $size, $angle, $c1, $c2, $strokecolor, $fontfile, $text);
        return imagefttext($image, $size, $angle, $x, $y, $textcolor, $fontfile, $text);
    }
}
