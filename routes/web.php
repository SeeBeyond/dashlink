<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::inertia('/', 'Beta/Home')->name('dashboard');
Route::inertia('/help', 'Beta/Docs/Index');
Route::inertia('/faq', 'Beta/Docs/Help');
Route::inertia('/privacy', 'Beta/Docs/ProvacyPolicy');
Route::inertia('/help/terms', 'Beta/Docs/TermsOfService');
Route::inertia('/basedball', 'Beta/BasedBalls');

Route::inertia('/settings/email', 'Beta/Settings/Email')->middleware('auth');
Route::inertia('/settings/password', 'Beta/Settings/Password')->middleware('auth');
Route::inertia('/settings', 'Beta/Settings/Index')->middleware('auth');
Route::get('/settings/profile', [ProfileController::class, 'edit'])->middleware('auth');
Route::post('/settings/profile', [ProfileController::class, 'update'])->middleware('auth');

Route::get('/card/{name}', [\App\Actions\CreateCard::class, 'show']);

Route::inertia('/link', 'Beta/Link')->middleware(['auth', 'verified', 'unlinked']);

//Route::get('/developers', [\App\Http\Controllers\ClientController::class, 'index'])->middleware(['auth', 'verified']);
//Route::post('/developers/request', [\App\Http\Controllers\ClientController::class, 'update'])->middleware(['auth', 'verified']);
//Route::inertia('/developers', 'Beta/Developers')->middleware(['auth', 'verified']);
Route::inertia('/admin', 'Beta/Admin')->middleware(['auth', 'verified', 'role:admin']);
Route::post('/admin', [\App\Http\Controllers\AdminController::class, '__invoke'])->middleware(['auth', 'verified', 'role:admin']);;

Route::get('/settings/connections', [\App\Http\Controllers\ConnectionController::class, 'index'])->middleware(['auth', 'verified']);
Route::delete('/settings/connection/{token}', [\App\Http\Controllers\ConnectionController::class, 'destroy'])->middleware(['auth', 'verified']);

Route::get('/settings/sessions', function () {
    Inertia::render('Beta/Settings/Sessions', [
        'sessions' => (new App\Http\Controllers\UserProfileController)->sessions(request())->all()
    ]);
})->middleware(['auth', 'verified']);
Route::inertia('/settings/2fa', 'Beta/Settings/TwoFactorAuthentication')->middleware(['auth', 'verified']);

Route::post('/proxy/login', [ProfileController::class, 'store']);

Route::group([
    'prefix' => 'settings',
    'as' => 'settings::',
    'middleware' => ['auth', 'verified',],
    'namespace' => 'App\Http\Controllers'
],
    function () {

        /*
         * User & Profile
         */
        /*
        Route::get('/profile', [
            'uses' => 'UserProfileController@show',
            'as' => 'profile.show',
        ]);
        */

        Route::delete('/sessions', [
            'uses' => 'OtherBrowserSessionsController@destroy',
            'as' => 'sessions.destroy'
        ]);

        Route::delete('/user', [
            'uses' => 'CurrentUserController@destroy',
            'as' => 'user.destroy',
        ]);

        /*
         * Personal Access
         */
        /*
        Route::get('/tokens', [ApiTokenController::class, 'index'])->name('tokens.index');
        Route::post('/tokens', [ApiTokenController::class, 'store'])->name('tokens.store');
        Route::put('/tokens/{token}', [ApiTokenController::class, 'update'])->name('tokens.update');
        Route::delete('/tokens/{token}', [ApiTokenController::class, 'destroy'])->name('tokens.destroy');
        */
    });


// Profile Information...
Route::put('/user/profile-information', [
    'uses' => 'App\Http\Controllers\ProfileInformationController@update',
    'middleware' => 'auth',
    'as' => 'auth::user-profile-information.update',
]);

require_once 'passport.php';
