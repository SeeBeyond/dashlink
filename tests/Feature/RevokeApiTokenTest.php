<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Tests\TestCase;

class RevokeApiTokenTest extends TestCase
{
    use RefreshDatabase;

    public function test_api_tokens_can_be_deleted()
    {
        $this->markTestSkipped('must be revisited.');
        $this->actingAs($user = User::factory()->create());

        $token = $user->createToken('Test Token', ['create', 'read'])->token;

        $response = $this->delete(route('settings::tokens.destroy', $token->id));

        $this->assertCount(0, $user->fresh()->tokens()->where('revoked', false)->get());
    }

    public function setUp(): void {
        parent::setUp();
        $this->artisan('passport:install');
    }
}
