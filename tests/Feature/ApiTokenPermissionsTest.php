<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Tests\TestCase;

class ApiTokenPermissionsTest extends TestCase
{
    use RefreshDatabase;

    public function test_api_token_permissions_can_be_updated()
    {
        $this->markTestSkipped('must be revisited.');
        $this->actingAs($user = User::factory()->create());

        $token = $user->createToken('Test Token', ['create', 'read'])->token;

        $response = $this->put(route('settings::tokens.update', $token->id), [
            'name' => $token->name,
            'permissions' => [
                'delete',
                'missing-permission',
            ],
        ]);

        $this->assertTrue($user->fresh()->tokens->first()->can('delete'));
        $this->assertFalse($user->fresh()->tokens->first()->can('read'));
        $this->assertFalse($user->fresh()->tokens->first()->can('missing-permission'));
    }

    public function setUp(): void {
        parent::setUp();
        $this->artisan('passport:install');
    }
}
